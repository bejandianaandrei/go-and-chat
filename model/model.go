package model

// User is representing user structure
type User struct {
	ID       string
	Username string
	Password string
}

// Token is the structure of the AUTH value
type Token struct {
	Value     string
	Timestamp string
}

// UserToken is the structure of the AUTH value
type UserToken struct {
	User  *User
	Token *Token
}

// Members structure represents the mapping between Users and Rooms
type Members struct {
	User *User
	Room *ChatRoom
}

// ChatRoom represents the rooms in the chat
type ChatRoom struct {
	ID               string
	Name             string
	CreatedTimestamp string
}

// Message is the structure of the message object
type Message struct {
	ID        string
	User      *User
	ChatRoom  *ChatRoom
	Value     string
	Photo     string
	Timestamp string
}
