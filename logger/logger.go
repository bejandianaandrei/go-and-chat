package logger

import (
	"os"

	"github.com/withmandala/go-log"
)

var clogger *log.Logger
var logger *log.Logger

// Logger struct encapsulates a go-log logger
type Logger struct {
	logger *log.Logger
}

// CreateColoredLogger creates a loger with colored output
func CreateColoredLogger() *Logger {
	if clogger == nil {
		clogger = log.New(os.Stderr).WithColor().WithTimestamp().WithDebug()
	}
	return &Logger{clogger}
}

// CreateSimpleLogger creates a simple logger
func CreateSimpleLogger() *Logger {
	if logger == nil {
		logger = log.New(os.Stderr).WithDebug().WithTimestamp()
	}
	return &Logger{logger}
}

// Logger method returns the logger field
func (l *Logger) Logger() *log.Logger {
	return l.logger
}
