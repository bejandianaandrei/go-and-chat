package main

import (
	"context"
	"fmt"
)

func change(ctx context.Context) {
	context.WithValue(ctx, "k", "")
}

func call(ctx context.Context) {
	change(ctx)
	fmt.Printf("%v", ctx.Value("k"))
}
func main() {
	call(context.Background())

}
