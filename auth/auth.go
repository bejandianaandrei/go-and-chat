package auth

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"strings"
	"time"

	"github.com/dianabejan/chat/model"
)

// GenToken generates the auth token for user based on context
func GenToken(ctx context.Context) (model.Token, error) {
	username, present := ctx.Value("username").(string)
	if !present {
		return model.Token{Value: "", Timestamp: "-1"}, errors.New("Username is not present in the context")
	}
	token := createTokenStruct(username)
	return token, nil
}

// IsValidToken Checks if the token is associated with the user
func IsValidToken(ctx context.Context, f func(model.User, string) bool) (bool, error) {
	user, present := ctx.Value("user").(model.User)
	if !present {
		return false, errors.New("Username is not present in the context, cannot validate")
	}
	token, present := ctx.Value("token").(string)
	if !present {
		return false, errors.New("Token is not present in the context, cannot validate")
	}
	valid := f(user, token)
	return valid, nil
}

// createTokenStruct creates token base on timestamp
func createTokenStruct(username string) model.Token {

	timestamp := time.Now().Format(time.RFC850)
	h := md5.New()
	h.Write([]byte(strings.ToLower(username)))
	h.Write([]byte(strings.ToLower(timestamp)))
	value := hex.EncodeToString(h.Sum(nil))

	return model.Token{Value: value, Timestamp: timestamp}
}

// Crypt creates the md5 token from a key
func Crypt(key string) string {

	h := md5.New()
	h.Write([]byte(key))
	value := hex.EncodeToString(h.Sum(nil))

	return value
}
