package main

import (
	"html/template"
	"net/http"
	"os"

	"github.com/dianabejan/chat/client/client"
	"github.com/withmandala/go-log"
)

var logger = log.New(os.Stderr).WithColor().WithDebug()

func main() {
	c := client.Connect()

	tmpl := template.Must(template.ParseFiles("client/templates/index.html"))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl.Execute(w, c)
	})
	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		logger.Fatal(err.Error())
	}
}
