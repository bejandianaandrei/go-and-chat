package client

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"os"

	"github.com/dianabejan/chat/proto"
	"github.com/gorilla/securecookie"
	"github.com/withmandala/go-log"
	"google.golang.org/grpc"
)

var logger = log.New(os.Stderr).WithColor().WithDebug()

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

// Client is a structure that encapsulates the gRPC Client
type Client struct {
}

// Connect function craetes a new client that encapsulates the chat client
func Connect() *Client {

	client := &Client{}
	AddHandlers()
	return client
}

func AddHandlers() {
	handleLogin()
	handleLogout()
	handleRooms()
}
func setSession(token string, response http.ResponseWriter) {
	value := map[string]string{
		"token": token,
	}
	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(response, cookie)
	}
}

func clearSession(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}

func handleRooms() {
	http.HandleFunc("/rooms", func(response http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.ParseFiles("client/templates/rooms.html"))
		tmpl.ExecuteTemplate(response, "rooms", nil)
	})
}

func handleLogout() {
	http.HandleFunc("/logout", func(response http.ResponseWriter, request *http.Request) {
		clearSession(response)
		http.Redirect(response, request, "/", 302)
	})
}

func handleLogin() {
	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		target := "/rooms"
		if err != nil {
			logger.Fatal(err.Error())
		}
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		if username == "" || password == "" {
			fmt.Fprintf(w, "Password or username not provided")
		} else {
			req := &proto.LogInRequest{Username: username, Password: password}

			conn, err := grpc.Dial("localhost:7777", grpc.WithInsecure())
			if err != nil {
				logger.Debugf("Connection to localhost can't be established %v", err)
				target = "/"
			}
			defer conn.Close()
			c := proto.NewChatClient(conn)
			rsp, err := c.LogIn(context.Background(), req)
			if err != nil {
				logger.Info(err.Error())
				fmt.Fprintf(w, "Password or username are incorrect")
				target = "/"
			}
			token := rsp.GetToken()
			setSession(token, w)
			http.Redirect(w, r, target, 302)
		}
	})

}
