package main

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"os"

	"github.com/dianabejan/chat/dao"
	_ "github.com/mattn/go-sqlite3"
	"github.com/withmandala/go-log"
)

var logger = log.New(os.Stderr).WithColor().WithDebug()

func main() {
	connection, err := dao.Connect()

	if err != nil {
		logger.Errorf("Error opening database: %v", err.Error())
	}
	defer connection.Close()

	createUserTable(connection.Database)
	createRoomTable(connection.Database)
	createMembersTable(connection.Database)
	createMessageTable(connection.Database)
	createTokenTable(connection.Database)
	createUserTokenTable(connection.Database)

}
func dummyUser(database *sql.DB) error {
	statement, err := database.Prepare("INSERT INTO  user (username, password) values (?, ?);")
	if err != nil {
		logger.Errorf("Error craete  statement: %v", err.Error())
		return err
	}
	h := md5.New()
	h.Write([]byte("dianna"))
	hashedPass := hex.EncodeToString(h.Sum(nil))
	_, err = statement.Exec("dianna", hashedPass)

	if err != nil {
		logger.Errorf("Error exec  statement: %v", err.Error())
		return err
	}
	return nil
}
func createUserTable(database *sql.DB) error {

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS user ( id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(64), password VARCHAR(64), CONSTRAINT username_unique UNIQUE (username));")
	if err != nil {
		logger.Errorf("Error creating `CREATE USER TABLE` statement: %v", err.Error())
		return err
	}

	result, err := statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE USER TABLE` statement: %v", err.Error())
		return err
	}

	id, err := result.LastInsertId()
	if err != nil {
		logger.Errorf("Error executing `LAST INSERTED ID IN USER TABLE`: %v", err.Error())
		return err
	}

	logger.Infof("Success `CREATE USER TABLE`, last inserted id %d", id)
	return nil
}

func createRoomTable(database *sql.DB) error {
	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS chatRoom ( id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(64), CONSTRAINT roomname_unique UNIQUE (roomname));")
	if err != nil {
		logger.Errorf("Error creating `CREATE CHATROOM TABLE` statement: %v", err.Error())
		return err
	}

	result, err := statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE CHATROOM TABLE` statement: %v", err.Error())
		return err
	}

	id, err := result.LastInsertId()
	if err != nil {
		logger.Errorf("Error executing `LAST INSERTED ID IN CHATROOM TABLE`: %v", err.Error())
		return err
	}

	logger.Infof("Success `CREATE CHATROOM TABLE`, last inserted id %d", id)
	return nil
}

func createMessageTable(database *sql.DB) error {

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS message ( id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER, roomId INTEGER, value VARCHAR(64) NULL, photo VARCHAR(64) NULL, timestamp VARCHAR(64) NULL, FOREIGN KEY(userId) REFERENCES user(id), FOREIGN KEY(roomId) REFERENCES charRoom(id));")
	if err != nil {
		logger.Errorf("Error creating `CREATE MESSAGE TABLE` statement: %v", err.Error())
		return err
	}

	result, err := statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE MESSAGE TABLE` statement: %v", err.Error())
		return err
	}

	id, err := result.LastInsertId()
	if err != nil {
		logger.Errorf("Error executing `LAST INSERTED ID IN MESSAGE TABLE`: %v", err.Error())
		return err
	}

	logger.Infof("Success `CREATE MESSAGE TABLE`, last inserted id %d", id)
	return nil
}

func createMembersTable(database *sql.DB) error {

	statement, err := database.Prepare("CREATE TABLE  IF NOT EXISTS members ( userId INTEGER, roomId INTEGER, FOREIGN KEY(userId) REFERENCES user(id), FOREIGN KEY(roomId) REFERENCES charRoom(id), PRIMARY KEY (userId, roomId));")
	if err != nil {
		logger.Errorf("Error creating `CREATE MEMBERS TABLE` statement: %v", err.Error())
		return err
	}

	result, err := statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE MEMBERS TABLE` statement: %v", err.Error())
		return err
	}

	id, err := result.LastInsertId()
	if err != nil {
		logger.Errorf("Error executing `LAST INSERTED ID IN MEMBERS TABLE`: %v", err.Error())
		return err
	}

	logger.Infof("Success `CREATE MEMBERS TABLE`, last inserted id %d", id)
	return nil
}

func createTokenTable(database *sql.DB) error {

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS token ( value VARCHAR(256), timestamp VARCHAR(256), CONSTRAINT token_unique UNIQUE (value));")
	if err != nil {
		logger.Errorf("Error creating `CREATE TOKEN TABLE` statement: %v", err.Error())
		return err
	}

	_, err = statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE TOKEN TABLE` statement: %v", err.Error())
		return err
	}

	logger.Info("Success `CREATE TOKEN TABLE`")
	return nil
}

func createUserTokenTable(database *sql.DB) error {

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS userToken ( userId INTEGER, token VARCHAR(256), FOREIGN KEY(userId) REFERENCES user(id), FOREIGN KEY(token) REFERENCES token(value), PRIMARY KEY (userId, token));")
	if err != nil {
		logger.Errorf("Error creating `CREATE USERTOKEN TABLE` statement: %v", err.Error())
		return err
	}
	_, err = statement.Exec()
	if err != nil {
		logger.Errorf("Error executing `CREATE USERTOKEN TABLE` statement: %v", err.Error())
		return err
	}

	logger.Info("Success `CREATE USERTOKEN TABLE`")
	return nil
}
