package dao

import (
	"context"
	"database/sql"

	"github.com/dianabejan/chat/logger"
	_ "github.com/mattn/go-sqlite3" //imported idk whyNub
)

const dbType string = "sqlite3"
const dbName string = "./chat.db"

var log = logger.CreateColoredLogger()

// Connection is a class that holds the database connection
type Connection struct {
	Database *sql.DB
	Context  context.Context
}

// Connect method returns a connection to SQLite
func Connect() (*Connection, error) {
	db, err := sql.Open(dbType, dbName)
	if err != nil {
		log.Logger().Errorf("Cannot open database connection: %v", err.Error())
	}
	log.Logger().Info("Database open")
	return &Connection{db, context.Background()}, nil
}

// Close method closes the connection to database
func (c *Connection) Close() {
	c.Database.Close()
}
