package dao

import (
	"context"
	"net/http"

	"github.com/dianabejan/chat/model"
	"github.com/dianabejan/chat/proto"
)

// AddUser method adds an user to database
func (c *Connection) AddUser(ctx context.Context, in *proto.AddUserRequest) (*proto.AddUserResponse, error) {

	log.Logger().Info("Started executing insert user")
	stmt, err := c.Database.Prepare("INSERT INTO user(username, password) values(?,?)")
	if err != nil {
		log.Logger().Errorf("Error creating insert user statement: %v", err.Error())
		return &proto.AddUserResponse{Status: http.StatusBadRequest}, err
	}

	_, err = stmt.Exec(in.Username, in.Password)
	if err != nil {
		log.Logger().Errorf("Error executing insert user statement: %v", err.Error())
		return &proto.AddUserResponse{Status: http.StatusBadRequest}, err
	}

	log.Logger().Info("Finished executing insert user")

	return &proto.AddUserResponse{Status: http.StatusCreated}, nil
}

// LogIn method checks in the database if the user exists and returns the ID or error
func (c *Connection) LogIn(ctx context.Context, in *proto.LogInRequest) (*proto.LogInResponse, error) {

	log.Logger().Info("[Start] User login")

	rows, err := c.Database.QueryContext(c.Context, "SELECT id FROM user WHERE username = ? AND password = ?;", in.Username, in.Password)
	if err != nil {
		log.Logger().Errorf("Failed creating login statement: %v", err.Error())
		return &proto.LogInResponse{Status: http.StatusBadRequest}, err
	}

	rows.Next()
	var sid string
	if err := rows.Scan(&sid); err != nil {
		log.Logger().Errorf("Failed getting id: %v", err.Error())
		return &proto.LogInResponse{Status: http.StatusUnauthorized}, err
	}

	ctx = context.WithValue(ctx, "userID", sid)

	log.Logger().Info("[Finish] user login")

	return &proto.LogInResponse{Status: http.StatusOK}, nil
}

// AddToken method adds an user to database
func (c *Connection) AddToken(token model.Token) error {

	log.Logger().Info("Started executing insert token")
	stmt, err := c.Database.Prepare("INSERT INTO token(value, timestamp) values(?,?)")
	if err != nil {
		log.Logger().Errorf("Error creating insert token statement: %v", err.Error())
	}

	_, err = stmt.Exec(token.Value, token.Timestamp)
	if err != nil {
		log.Logger().Errorf("Error executing insert token statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing insert token")

	return nil
}

// DeleteToken method adds an user to database
func (c *Connection) DeleteToken(token model.Token) error {

	log.Logger().Info("Started executing delete token")
	stmt, err := c.Database.Prepare("DELETE FROM token where value = ? AND timestamp = ?")
	if err != nil {
		log.Logger().Errorf("Error creating delete token statement: %v", err.Error())
	}

	_, err = stmt.Exec(token.Value, token.Timestamp)
	if err != nil {
		log.Logger().Errorf("Error executing delete token statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing delete token")

	return nil
}

// AddUserToken method adds an association between user and token
func (c *Connection) AddUserToken(token, userID string) error {

	log.Logger().Info("Started executing insert userToken")
	stmt, err := c.Database.Prepare("INSERT INTO userToken(token, userId) VALUES (?, ?);")
	if err != nil {
		log.Logger().Errorf("Error creating insert userToken statement: %v", err.Error())
	}

	_, err = stmt.Exec(token, userID)
	if err != nil {
		log.Logger().Errorf("Error executing insert userToken statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing insert userToken")

	return nil
}

// DeleteUserToken method deletes an user token from the database
func (c *Connection) DeleteUserToken(token, userID string) error {

	log.Logger().Info("Started executing delete userToken")
	stmt, err := c.Database.Prepare("DELETE FROM userToken where token = ? AND userId = ?")
	if err != nil {
		log.Logger().Errorf("Error creating delete userToken statement: %v", err.Error())
	}

	_, err = stmt.Exec(token, userID)
	if err != nil {
		log.Logger().Errorf("Error executing delete userToken statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing delete userToken")

	return nil
}

// DeleteUserToken method deletes an user token from the database
func (c *Connection) CheckExistsUserToken(token, userID string) error {

	log.Logger().Info("Started executing  check exists userToken")
	rows, err := c.Database.QueryContext(c.Context, "SELECT * FROM userToken where token = ? AND userId = ?")
	if err != nil {
		log.Logger().Errorf("Error executing select userToken statement: %v", err.Error())
	}

	rows.Next()

	if err := rows.Scan(); err != nil {
		log.Logger().Errorf("Failed getting id: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing check exists userToken")

	return nil
}

// AddMember method adds a M:N relationship  between user and room
func (c *Connection) AddMember(roomID, userID string) error {

	log.Logger().Info("Started executing insert member")
	stmt, err := c.Database.Prepare("INSERT INTO member(roomId, userId) VALUES (?, ?);")
	if err != nil {
		log.Logger().Errorf("Error creating insert member statement: %v", err.Error())
	}

	_, err = stmt.Exec(roomID, userID)
	if err != nil {
		log.Logger().Errorf("Error executing insert member statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing insert member")

	return nil
}

// DeleteMember method deletes an user from the database
func (c *Connection) DeleteMember(token, userID string) error {

	log.Logger().Info("Started executing delete userToken")
	stmt, err := c.Database.Prepare("DELETE FROM userToken where token = ? AND userId = ?")
	if err != nil {
		log.Logger().Errorf("Error creating delete userToken statement: %v", err.Error())
	}

	_, err = stmt.Exec(token, userID)
	if err != nil {
		log.Logger().Errorf("Error executing delete userToken statement: %v", err.Error())
		return err
	}

	log.Logger().Info("Finished executing delete userToken")

	return nil
}
