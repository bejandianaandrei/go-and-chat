package main

import (
	"log"
	"net"

	"github.com/dianabejan/chat/proto"
	"github.com/dianabejan/chat/server/server"
	"google.golang.org/grpc"
)

const port = ":7777"

func main() {
	listener, err := net.Listen("tcp", port)

	if err != nil {
		log.Fatalf("Unable to listen to specified port: %s \n %v", port, err)
	}
	srv := grpc.NewServer()
	serve := server.NewServer()
	proto.RegisterChatServer(srv, serve)
	err = srv.Serve(listener)
}
