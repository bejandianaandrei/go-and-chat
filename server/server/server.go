package server

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"net/http"
	"strings"

	"github.com/dianabejan/chat/auth"
	"github.com/dianabejan/chat/dao"
	"github.com/dianabejan/chat/logger"
	"github.com/dianabejan/chat/model"
	"github.com/dianabejan/chat/proto"
	"golang.org/x/net/context"
)

var log = logger.CreateColoredLogger()

// Server structure that encapsulates the connection to database
type Server struct {
	Conn *dao.Connection
}

func NewServer() *Server {
	dao, _ := dao.Connect()
	return &Server{dao}
}

func (s *Server) AddMessage(ctx context.Context, in *proto.AddMessageRequest) (*proto.AddMessageResponse, error) {

	return new(proto.AddMessageResponse), nil
}

func (s *Server) GetMessages(ctx context.Context, in *proto.GetMessagesRequest) (*proto.GetMessagesResponse, error) {
	return new(proto.GetMessagesResponse), nil
}

func (s *Server) DeleteMessage(ctx context.Context, in *proto.DeleteMessageRequest) (*proto.DeleteMessageResponse, error) {
	return new(proto.DeleteMessageResponse), nil
}

func (s *Server) AddRoom(ctx context.Context, in *proto.AddRoomRequest) (*proto.AddRoomResponse, error) {
	return new(proto.AddRoomResponse), nil
}

func (s *Server) GetRoom(ctx context.Context, in *proto.GetRoomRequest) (*proto.GetRoomResponse, error) {
	return new(proto.GetRoomResponse), nil
}

func (s *Server) GetRooms(ctx context.Context, in *proto.GetRoomsRequest) (*proto.GetRoomsResponse, error) {
	return new(proto.GetRoomsResponse), nil
}

func (s *Server) JoinRoom(ctx context.Context, in *proto.JoinRoomRequest) (*proto.JoinRoomResponse, error) {
	return new(proto.JoinRoomResponse), nil
}

func (s *Server) LeaveRoom(ctx context.Context, in *proto.LeaveRoomRequest) (*proto.LeaveRoomResponse, error) {
	return new(proto.LeaveRoomResponse), nil
}

func (s *Server) DeleteRoom(ctx context.Context, in *proto.DeleteRoomRequest) (*proto.DeleteRoomResponse, error) {
	return new(proto.DeleteRoomResponse), nil
}

// AddUser method adds user to the database
func (s *Server) AddUser(ctx context.Context, in *proto.AddUserRequest) (*proto.AddUserResponse, error) {

	pass := in.GetPassword()
	uname := in.GetUsername()
	a := 1

	if len(pass) < 6 {
		return &proto.AddUserResponse{Status: http.StatusBadRequest}, errors.New("password is too short")
	}

	if len(uname) < 6 {
		return &proto.AddUserResponse{Status: http.StatusBadRequest}, errors.New("password is too short")
	}

	h := md5.New()
	h.Write([]byte(strings.ToLower(in.Password)))
	hashedPass := hex.EncodeToString(h.Sum(nil))

	rsp, err := s.Conn.AddUser(model.User{Password: hashedPass, Username: uname})

	if err != nil {
		return &proto.AddUserResponse{Status: http.StatusBadRequest}, err
	}

	return rsp, nil
}

func (s *Server) GetMembers(ctx context.Context, in *proto.GetMembersRequest) (*proto.GetMembersResponse, error) {
	return new(proto.GetMembersResponse), nil
}

// LogIn method performs an login with given credentials
func (s *Server) LogIn(ctx context.Context, in *proto.LogInRequest) (*proto.LogInResponse, error) {
	pass := in.GetPassword()
	uname := in.GetUsername()

	if len(pass) < 6 {
		return &proto.LogInResponse{Status: http.StatusBadRequest}, errors.New("password is too short")
	}

	if len(uname) < 6 {
		return &proto.LogInResponse{Status: http.StatusBadRequest}, errors.New("username is too short")
	}

	h := md5.New()
	h.Write([]byte(in.Password))
	hashedPass := hex.EncodeToString(h.Sum(nil))

	rsp, err := s.Conn.LogIn(ctx, in)
	if err != nil {
		return rsp, err
	}

	token, err := auth.GenToken(ctx)
	if err != nil {
		return rsp, err
	}
	s.Conn.AddToken(token)
	s.Conn.AddUserToken(ctx)

	rsp.Token = token
	return &proto.LogInResponse{Status: http.StatusAccepted}, nil
}

func (s *Server) LogOut(ctx context.Context, in *proto.LogOutRequest) (*proto.LogOutResponse, error) {
	return new(proto.LogOutResponse), nil
}
